var animation = lottie.loadAnimation({
    container: document.getElementById('logo'), // the dom element that will contain the animation
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'data/data.json' // the path to the animation json
});
